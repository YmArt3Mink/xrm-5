#pragma once

#include <string>


struct ChatMessage
{
	std::string Text;
	std::string Author;
	std::string Addressee;

	ChatMessage(
		std::string sText,
		std::string sAuthor,
		std::string sAddressee
		)
	{
		this->Text		= sText;
		this->Author    = sAuthor;
		this->Addressee = sAddressee;
	}
	
	~ChatMessage()
	{}
};

