#pragma once

#include "ChatClient.h"


class AbstractChatClientDecorator 
	: public AbstractChatClient
{
protected:
	const AbstractChatClient* const _pClient;

public:
	AbstractChatClientDecorator(AbstractChatClient* pClient) 
		: _pClient(pClient)
	{}

	virtual 
	~AbstractChatClientDecorator()
	{
		delete _pClient;
	}

public:
	virtual void
	SendMessage(
		ChatMessage* pMessage,
		std::ostream& stream
		) const
	{
		auto pNewMessage = PreSendHandlerFunc(pMessage);
		_pClient->SendMessage(pNewMessage, stream);
	}

	virtual void
	ReceiveMessage()
	{
		return;
	}

	virtual ChatMessage*
	PreSendHandlerFunc(ChatMessage* pMessage) const = 0;
};

