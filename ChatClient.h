#pragma once

#include <iostream>

#include "AbstractChatClient.h"


class ChatClient 
	: public AbstractChatClient
{
public:
	ChatClient()
	{}

	~ChatClient()
	{}

public:
	void
	SendMessage(
		ChatMessage* pMessage,
		std::ostream& stream
		) const
	{
		stream << "\nAuthor: "	 << pMessage->Author;
		stream << "\nAddressee: " << pMessage->Addressee;
		stream << "\nMessage: "	 << pMessage->Text;
		stream << std::endl;
	}

	void
	ReceiveMessage()
	{
		return;
	}
};

