#pragma once

#include "ChatMessage.h"


class AbstractChatClient
{
public:
	virtual 
	~AbstractChatClient() 
	{}

	virtual void
	SendMessage(
		ChatMessage* pMessage, 
		std::ostream& stream
		) const = 0;
	
	virtual void
	ReceiveMessage() = 0;
};

