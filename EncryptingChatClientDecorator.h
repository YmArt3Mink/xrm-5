#pragma once

#include "AbstractChatClientDecorator.h"


class EncryptingChatClientDecorator
	: public AbstractChatClientDecorator
{
public:
	EncryptingChatClientDecorator(AbstractChatClient* pClient)
		: AbstractChatClientDecorator(pClient)
	{}

	~EncryptingChatClientDecorator()
	{}

public:
	ChatMessage*
	PreSendHandlerFunc(ChatMessage* pMessage) const
	{
		auto sMessageText = pMessage->Text;
		sMessageText = 
			"<encrypted>" 
				+ sMessageText
				+ "</encrypted>";

		return
			new ChatMessage(
				sMessageText,
				pMessage->Author,
				pMessage->Addressee
				);
	}
};