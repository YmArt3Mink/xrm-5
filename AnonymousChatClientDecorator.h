#pragma once

#include "AbstractChatClientDecorator.h"


class AnonymousChatClientDecorator 
	: public AbstractChatClientDecorator
{
public:
	AnonymousChatClientDecorator(AbstractChatClient* pClient)
		: AbstractChatClientDecorator(pClient)
	{}

	~AnonymousChatClientDecorator()
	{}

public:
	ChatMessage*
	PreSendHandlerFunc(ChatMessage* pMessage) const
	{
		auto sAuthor = pMessage->Author;
		sAuthor = 
			std::to_string(sAuthor.size()) 
				+ std::to_string(sAuthor.capacity());

		return
			new ChatMessage(
				pMessage->Text,
				sAuthor,
				pMessage->Addressee
				);
	}
};

