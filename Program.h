#pragma once

#include "AnonymousChatClientDecorator.h"
#include "EncryptingChatClientDecorator.h"
#include "ChatMessage.h"

class Program
{
private:
	static Program* _pInstance;

private:
	Program()
	{}

public:
	~Program()
	{
		delete _pInstance;
	}

	static Program* 
	GetInstance() 
	{
		if (_pInstance == nullptr)
		{
			_pInstance = new Program();
		}

		return _pInstance;
	}

public:
	void
	TestDecorator(AbstractChatClientDecorator* pDecorator)
	{
		auto pMessage =
			new ChatMessage(
				"Test_Text",
				"Test_Author",
				"Test_Addressee"
				);
		pDecorator->SendMessage(pMessage, std::cout);
	}

	void
	Run()
	{
		auto pClient = new ChatClient();
		TestDecorator(new AnonymousChatClientDecorator(pClient));
		TestDecorator(new EncryptingChatClientDecorator(pClient));
	}
};

